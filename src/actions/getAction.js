import { GET_NUMBERS_BASKET, GET_NUMBERS_BASKET_TO_PANEL } from './types'

export const getNumbers = () => {
        return(dispatch)=>{
          console.log("Getting all Basket Numbers");
          dispatch({
              type: GET_NUMBERS_BASKET,
          });
        }
    }
   
   
export const getNumbersToPanel = () => {
  return(dispatch)=>{
    console.log("Getting all Basket Panel Numbers");
    dispatch({
        type: GET_NUMBERS_BASKET_TO_PANEL,
    });
  }
}
