import { ADD_PRODUCT_BASKET, ADD_PRODUCT_BASKET_TO_PANEL } from './types'

export const addBasket  = (productName, quantity)=>{
    return(dispatch)=>{
      console.log("adding to Basket");
      console.log("Product: ", productName);
      dispatch({
          type: ADD_PRODUCT_BASKET,
          payload: productName, quantity
          
      });
    }
}


export const addBasketToPanel  = (productName)=>{
  return(dispatch)=>{
    console.log("adding to Basket Panel");
    console.log("Product: ", productName);
    dispatch({
        type: ADD_PRODUCT_BASKET_TO_PANEL,
        payload: productName
        
    });
  }
}