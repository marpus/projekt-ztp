import React, { Fragment } from 'react'
import { connect } from 'react-redux';


function AdminPanel({basketProps}) {
    console.log("basketPanelProps")
    console.log(basketProps);

    let productsInCartPanel = [];

    Object.keys(basketProps.products).forEach( function(item){
        console.log(item);
        console.log(basketProps.products[item].inCart);
        if(basketProps.products[item].inCart) {
          productsInCartPanel.push(basketProps.products[item])
        }
        console.log(productsInCartPanel);
    })

    productsInCartPanel = productsInCartPanel.map( (productsPanel ) => {
        console.log("My product is");
        console.log(productsPanel);
        return (
            <Fragment>
                <div className="product"><ion-icon name="close-circle"></ion-icon>
                    <span className="sm-hide">{productsPanel.name}</span>
                </div>
                <div className="price sm-hide">{productsPanel.tableNumber} </div>
                <div className="quantity">
                    <ion-icon className="decrease" name="arrow-back-circle-outline"></ion-icon>
                    <span>{productsPanel.numbers}</span>
                    <ion-icon className="increase" name="arrow-forward-circle-outline"></ion-icon>
                </div>
                <div className="total">${productsPanel.numbers * productsPanel.price},00</div>
            </Fragment>

        )
    })

    return (
        <div className="container-products">
            <div className="product-header">
                <h5 className="product-title">PRODUCT</h5>
                <h5 className="price sm-hide">Table</h5>
                <h5 className="quantity">QUANTITY</h5>
                <h5 className="total">TOTAL</h5>
                

            </div>
            <div className="products">
                { productsInCartPanel }
            </div>
            <div className="basketTotalContainer">
                <h4 className="basketTotalTitle">Basket Total</h4>
                <h4 className="basketTotal">{basketProps.cartCost},00</h4>
            </div>
        </div>

    )
}

const mapStateToProps = state => ({
    basketProps: state.basketState 
});

export default connect (mapStateToProps)(AdminPanel);
