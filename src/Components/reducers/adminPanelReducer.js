import { ADD_PRODUCT_BASKET_TO_PANEL, GET_NUMBERS_BASKET_TO_PANEL } from '../../actions/types'

const initialState = {
    basketNumbers: 0,
    cartCost: 0,
    tableNumber: 0,
    productsPanel: {
        
    }
}

export default (state = initialState, action) => {
    switch(action.type){
        case GET_NUMBERS_BASKET_TO_PANEL:
            let addQuantity = { ...state.productsPanel[action.payload]}

            addQuantity.numbers += 1;
            addQuantity.inCart = true;
            console.log(addQuantity);

            return {
                ...state,
                basketNumbers: state.basketNumbers + 1,
                cartCost: state.cartCost + state.productsPanel[action.payload].price,
                productsPanel: {
                    ...state.productsPanel,
                    [action.payload]: addQuantity,

                }
            }
            case ADD_PRODUCT_BASKET_TO_PANEL:
                return {
                    ...state
                }
        default:
            return state;
    }
}