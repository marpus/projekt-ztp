import React, { useEffect } from 'react';
import logo from '../../Images/logo1.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartPlus } from '@fortawesome/free-solid-svg-icons'
import './Header.css'

import {connect } from 'react-redux';
import { getNumbers } from '../../actions/getAction';
import { Link } from 'react-router-dom'

const Header = (props) => {
    console.log(props);

    useEffect(() => {
        getNumbers();
    }, []);


    return (
        <div>
            {/* <div>
                <p className='site-notice bg-light'> <b> Site work is going on, Work will be done asap. </b></p>
            </div> */}
            <nav className="navbar navbar-expand-lg navbar-light">
                <Link to="/"><img className='site-logo' src={logo} alt="" /></Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>


                <div className="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                    <ul className="navbar-nav nav_links">
                        <li className="nav-item active">
                            <Link className="nav-link" to ="/cart"><FontAwesomeIcon icon={faCartPlus} />{props.basketProps.basketNumbers}<span className="sr-only">(current)</span></Link>
                        </li>
                        <li className="nav-item active">
                            <Link className="nav-link" to ="/login">Login <span className="sr-only">(current)</span></Link>
                        </li>
                        <li className="nav-item active">
                            <Link to ="/signup"><button className='btn-login'>Sign Up</button></Link>
                        </li>
                        <Link to ="/adminpanel"><button className='btn-login'>Admin Panel</button></Link>
                    </ul>
                </div>
            </nav>
        </div>
    );
};

const mapStateToProps = state => ({
    basketProps: state.basketState
})
export default connect(mapStateToProps, { getNumbers })(Header);